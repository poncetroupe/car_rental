import './App.css';
import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Home from './pages/Home.js';
import Userform from './pages/Userform.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import Admin from './pages/Admin.js';
import Cars from './pages/Cars.js';
import Nav_bar from './component/Nav_bar.js'
function App() {

  return (
    <Router>
      <Nav_bar />
      <Container >
        <Routes>
           <Route path="/Cars" element={<Cars  />} />
           <Route path="/Home" element={<Home  />} />
           <Route path="/Login" element={<Login  />} />
           <Route path="/Register" element={<Register />} />
           <Route path="/Userform" element={<Userform />} />
           <Route path="/Admin" element={<Admin />} />
           <Route path="/Logout" element={<Logout />} />
        </Routes>
      </Container>
    </Router>
  );
}

export default App;
