import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {Container} from 'react-bootstrap';


export default function Login() {
  return (
  	<Container style={{height: "87vh"}}>
      <div style={{marginTop: "15vh"}} className="d-flex flex-row justify-content-center">
        <Form style={{width: "30vw", background: "rgba(0,0,0,0.2)", border: "2px solid black", borderRadius: "10px"}}
         className="col-3 p-3 d-flex flex-column align-items-center">
         <h1 className="text-center mb-2">Log in as Admin</h1>
          <Form.Group style={{width: "35vh"}} className="mb-3">
            <Form.Label className="fw-bold">Email: </Form.Label>
            <Form.Control type="text" placeholder="Enter First Name"/>
          </Form.Group>
          <Form.Group style={{width: "35vh"}} className="mb-3">
            <Form.Label className="fw-bold">Password: </Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <Button variant="primary" type="submit" href="/Admin">Submit</Button>
        </Form>
      </div>
    </Container>
  );	
}








