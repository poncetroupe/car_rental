import React from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import { Container } from 'react-bootstrap';
import "../App.css";


export default function Userform() {
  return (
    <Container id="Userform" style={{height: "87vh"}}>
      <div style={{marginTop: "5vh"}} className="d-flex flex-row justify-content-center">
        <Form style={{width: "50vw", background: "rgba(0,0,0,0.2)", border: "2px solid black", borderRadius: "10px"}}
         className="col-3 p-3 d-flex flex-column align-items-center">
         <h1 className="text-center mb-2">Costumer Information Form</h1>
          <Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold"> Name: </Form.Label>
            <Form.Control type="text" placeholder="Enter Name"/>
          </Form.Group>
          <Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold">Contact Number: </Form.Label>
            <Form.Control type="Number" placeholder="Enter Contact Number" />
          </Form.Group>
          <Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold">Complete Home Address: </Form.Label>
            <Form.Control type="String" placeholder="Home address"/>
          </Form.Group>
          <Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold">Office Connected: </Form.Label>
            <Form.Control type="String" placeholder="Password" />
          </Form.Group>
          <Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold">Office Contact Number: </Form.Label>
            <Form.Control type="Number"/>
          </Form.Group> 
           <Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold">Office Address: </Form.Label>
            <Form.Control type="String"/>
          </Form.Group>  
          <Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold">Driver: </Form.Label>
            <Form.Control type="String"/>
          </Form.Group>  <
          Form.Group style={{width: "45vh"}} className="mb-3">
            <Form.Label className="fw-bold">Driver License: </Form.Label>
            <Form.Control type="text"/>
          </Form.Group> 
          <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
        </Form>
      </div>  
    </Container>
  );
}

