import Carousel from 'react-bootstrap/Carousel';

export default function Carousel_home() {
  return (
    <Carousel>
      <Carousel.Item interval={1000}>
        <img style={{Height: "600px", width: "100%"}} src="https://gomotors.net/blog/wp-content/uploads/2020/09/a966da0511edc2f3c595ae62f5c9ac95-1.jpg" alt=""/>
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={500}>
       <img style={{Height: "600px", width: "100%"}} src="https://s3.amazonaws.com/the-drive-staging/message-editor/1528475104876-1-bmwi8.jpg" alt=""/>
        <Carousel.Caption>  
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
       <img style={{Height: "600px", width: "100%"}} src="https://wallpapercave.com/wp/wp2153734.jpg" alt=""/>
        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  )
}
