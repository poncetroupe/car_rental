import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function CarItem() {
   return (
    <div className="col-3">
      <Card style={{ width: '300px', height:'300px', marginTop:"20px"}}>
        <Card.Img variant="top" src="holder.js/100px180" />
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <Button href="/Userform" variant="primary">Go Rent!</Button>
        </Card.Body>
      </Card> 
    </div> 
  );   
}

