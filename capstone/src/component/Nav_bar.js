import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import pic from '../pic/background.jpg';
import {Link} from 'react-router-dom';
import "../App.css";

export default function Nav_bar() {
  return (
    <Navbar id="Nav_Bar" style={{backgroundColor:"rgba(0, 0, 0, 0.3)", width: "100vw", margin:"0"}} expand="lg" className="d-flex flex-row">
     <Container>
        <Link to="/Login">
        <img style={{height: "100px"}} src={pic} />
        </Link>
        <Navbar.Brand style={{marginLeft:"32vh", fontSize: "24pt", fontWeight: "Bolder", color:"white"}} href="/Home">Aize Ace Rental Car</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav style={{marginLeft:"30vh", fontSize: "18pt", fontWeight: "Bolder", color:"white"}} className="me-auto">
            <Nav.Link href="/Cars">Cars</Nav.Link>
            <NavDropdown title="About us" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}