const mongoose = require("mongoose");

const carSchema = new mongoose.Schema({
	profile:{
		type: String,
		default: "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.svgrepo.com%2Fsvg%2F390416%2Fcar-travel-plus-add&psig=AOvVaw2xl1BUzbLWULpBKlr3f8WC&ust=1707973069400000&source=images&cd=vfe&opi=89978449&ved=0CBMQjRxqFwoTCPCp69KFqoQDFQAAAAAdAAAAABAR"
	},
	carName: {
		type: String,
		required: [true, "Carname is Required!!"]
	},
	description: {
		type: String,
		required: [true, "Description is Required!!"]
	},
	Price: {
		type: Number,
		required: [true, "Price is Required"]
	},
	seats: {
		type: Number,
		required: [true, "Seats is Required"]
	},
	availability: {
		type: Boolean,
		default: true
	},
	Driver: {
		type:Boolean,
		default: false
	},
	usersBooked: [
		{
			fullName: {
				type: String,
				required: [true, "Name is Required!"]
				
			},
			dateReserved: {
				type: Date,
			  	required: [true, "Reserve date is required"]
			}
		}
	]
})

module.exports = mongoose.model("Cars", carSchema);