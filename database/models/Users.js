const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "Name is Required!!"]
	},
	userName: {
		type: String,
		required: [true, "Username is Required!!"]
	},
	passWord: {
		type: String,
		required: [true, "Password is Required!!"]
	}
})

module.exports = mongoose.model("Users", userSchema);