const mongoose = require("mongoose");

const bookSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "Name is Required!!"]
	},
	carName: {
		type: String,
		required: [true, "Carname is Required!!"]
	},
	description: {
		type: String,
	},
	numDays: {
		type: Number,
		required: [true, "Days of booking is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is Required"]
	}
})

module.exports = mongoose.model("Book", bookSchema);