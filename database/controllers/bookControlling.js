const Book = require("../models/Book.js");

module.exports.addBook = (requestBody) => {
	let newBook = new Book({
		fullName: requestBody.fullName,
		carName: requestBody.carName,
		description: requestBody.description,
		numDays: requestBody.numDays,
		price: requestBody.price
	})

	return newBook.save().then((user, err) => {
		if(err){
			return false;
		} else {
			return true;
		}
	})
}