/*const Users = require("../models/Users.js");
const bcrypt = require("bcrypt");

module.exports.registerUser = (requestBody) => {
	let newUser = new Users({
		fullName: requestBody.fullName,
		userName: requestBody.userName,
		passWord: bcrypt.hashSync(requestBody.passWord, 10) 
	})

	return newUser.save().then((user, err) => {
		if(err){
			return false;
		} else {
			return true;
		}
	})
}*/
const Users = require("../models/Users.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (requestBody) => {
  let newUser = new Users({
    fullName: requestBody.fullName,
    userName: requestBody.userName,
    passWord: bcrypt.hashSync(requestBody.passWord, 10),
  });

  return newUser.save()
    .then((user) => {
      // The 'user' parameter here is the saved document
      console.log('User saved:', user);
      return true;  // or you can return the saved user document
    })
    .catch((err) => {
      console.error('Error saving user:', err);
      return false;
    });
};


module.exports.authenticateUser = (requestBody) => {
	return Users.findOne({userName: requestBody.userName}).then(result => {
		if (result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.passWord, result.passWord)
			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}