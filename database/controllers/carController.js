const Cars = require("../models/Cars.js");

module.exports.addCar = (requestBody) => {
	let newCar = new Cars({
		carName: requestBody.carName,
		description: requestBody.description,
		Price: requestBody.Price,
		seats: requestBody.seats

	})

	return newCar.save().then((user, err) => {
		if(err){
			return false;
		} else {
			return true;
		}
	})
}