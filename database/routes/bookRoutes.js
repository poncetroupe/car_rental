const express = require("express");
const router = express.Router();
const bookController = require("../controllers/bookControlling.js");

router.post("/addBook", (req, res) => {
	bookController.addBook(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;