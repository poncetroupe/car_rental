const express = require("express");
const router = express.Router();
const carController = require("../controllers/carController.js");

router.post("/addCar", (req, res) => {
	carController.addCar(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;